﻿using UnityEngine;
using UnityEngine.Collections;
using UnityEngine.Networking;

public class RegionSpawner2 : NetworkBehaviour
{

    public GameObject RegionPrefab;
    public Transform[] Regions;

    public override void OnStartServer()
    {
        for (int i = 0; i < Regions.Length; i++)
        {
            var Region = (GameObject)Instantiate(RegionPrefab, Regions[i].position, Regions[i].rotation);
            NetworkServer.Spawn(Region);
        }
    }
}
