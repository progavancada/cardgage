﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Networking;

public class DropZone : NetworkBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public int teste;
    [SyncVar(hook = "ChangeColorRegion")]
    public Color RegionActive = new Color(0, 0, 0);
    //[SyncVar(hook = "ChangeColorRegionteste")]
    //public int Regionteste = 0;
   
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("OnPointerEnter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("OnPointerExit");
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log(eventData.pointerDrag.name + "was dropped to " + gameObject.name);
        Drag d = eventData.pointerDrag.GetComponent<Drag>();

        if (d != null)
        {
            Debug.Log("RegionActive");
            d.parentToReturnTo = this.transform;
            //Regionteste = 2;

            RegionActive = d.GetComponent<Image>().color;
        }
    }

    public void ChangeColorRegion(Color RegionActive)
    {
        Debug.Log("ChangeColorRegion");
        MeshRenderer M = GetComponent<MeshRenderer>();
        Debug.Log("ChangeColorRegion");
        if (M != null)
        {

            M.material.color = RegionActive;
            Debug.Log("ChangeColorRegion");
        }
    }
    /*public void ChangeColorRegionteste(int Regionteste2)
    {
        Debug.Log("ChangeColorRegionteste");
        MeshRenderer M = Region.GetComponent<MeshRenderer>();
        Debug.Log("ChangeColorRegion");
        if (M != null)
        {
            if(Regionteste2 == 2) { M.material.color = Color.red; Regionteste = 0; }
            Debug.Log("ChangeColorRegion");
        }
    }
    */
}

