﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardGameManager : MonoBehaviour {

    public static CardGameManager control;

    public List<int> playerDeck;
    public List<int> opponentDeck;

    private int i;

    public List<int> WarriorDeck;
    public List<int> MageDeck;
    public List<int> RogueDeck;

    void Awake()
    {
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else if (control != this)
        {
            Destroy(gameObject);
        }
     }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Selecionar um Deck para o Jogador
    public void SeelectDeck(int SelectedDeck)
    {
        switch (SelectedDeck)
        {
            case 1:
                playerDeck = WarriorDeck;
                break;
            case 2:
                playerDeck = MageDeck;
                break;
            case 3:
                playerDeck = RogueDeck;
                break;
        }
    }
}
