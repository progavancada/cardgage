﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    public Transform parentToReturnTo = null;

    public float velocity = 3f;
    private Vector2 cardposition;
    private Vector2 cardpositionlocal;
    private List<RaycastResult> results;

    GameObject placeholder = null;

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");

        placeholder = new GameObject();
        placeholder.transform.SetParent(this.transform.parent);
        LayoutElement le = placeholder.AddComponent<LayoutElement>();
        le.preferredWidth = this.GetComponent<LayoutElement>().preferredWidth;
        le.preferredHeight = this.GetComponent<LayoutElement>().preferredHeight;
        le.flexibleWidth = 0;
        le.flexibleHeight = 0;

        placeholder.transform.SetSiblingIndex(this.transform.GetSiblingIndex());

        parentToReturnTo = this.transform.parent;
        //Debug.Log(this.transform.localPosition);
        //Debug.Log(this.transform.position);
        cardposition = this.transform.position;
        cardpositionlocal = new Vector2(-transform.localPosition.x, -transform.localPosition.y);
        this.transform.SetParent(this.transform.parent.parent, true);
        //Debug.Log(this.transform.localPosition);
        //Debug.Log(this.transform.position);
        this.transform.localPosition = cardpositionlocal;
        this.transform.position = cardposition;
        //Debug.Log("final" + this.transform.localPosition);
        //Debug.Log("final" + this.transform.position);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("OnDrag");

        this.transform.position = Vector3.MoveTowards(transform.position, eventData.position, velocity * Time.deltaTime);
        //StartCoroutine(ContinueAfterDrag(eventData.position, transform.position));
    }

    public void OnEndDrag(PointerEventData eventData)
    {

        //StartCoroutine(ContinueAfterDrag(eventData.position, transform.position));
        this.transform.SetParent(parentToReturnTo);
        this.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());

        GetComponent<CanvasGroup>().blocksRaycasts = true;

        List<RaycastResult> raycastHits = new List<RaycastResult>();

        Destroy(placeholder);
        EventSystem.current.RaycastAll(eventData, raycastHits);
        /*Debug.Log(results.Count);
        for (int i = 0; i < results.Count ; i++)
        {
            Debug.Log(results[i]);
        }
        */   
    }

    IEnumerator ContinueAfterDrag(Vector2 Destination, Vector2 CardPosition)
    {
        int acceleration = 200;
        CardPosition = this.transform.position;
        while (CardPosition != Destination)
        {
            this.transform.position = Vector3.MoveTowards(transform.position, Destination, acceleration * Time.deltaTime);
            CardPosition = this.transform.position;
            acceleration = acceleration + 20;
            
            yield return null;
        }
        
    }

}
