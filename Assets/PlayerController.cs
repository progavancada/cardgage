﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Collections;

public class PlayerController : NetworkBehaviour
{
    public Canvas playercanvas;
    public GameObject PlayerHand;
    public Button DrawButton;
    public GameObject Card;

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        var y = Input.GetAxis("Vertical") * Time.deltaTime * 150.0f;

        transform.Translate(x, 0, 0);
        transform.Translate(0, y, 0);

    }

    public override void OnStartLocalPlayer()
    {
        //Instantiate the hand
        var canvas = Instantiate(playercanvas);
        canvas.transform.SetParent(this.transform);

        PlayerHand = GameObject.FindGameObjectWithTag("Hand");
        Debug.Log(PlayerHand);
        foreach (Transform element in PlayerHand.transform){if (element.tag == "DrawButton"){DrawButton = element.GetComponent<Button>(); Debug.Log(DrawButton);}}
        Button btn = DrawButton.GetComponent<Button>();
        Debug.Log(btn);
        btn.onClick.AddListener(delegate () { Draw(); });


    }

    public void Draw()
    {
        var CardCopy = (GameObject)Instantiate(Card);
        CardCopy.GetComponent<CardBase>().cardIndex = CardGameManager.control.playerDeck[0];
        CardGameManager.control.playerDeck.RemoveAt(0);
        Vector3 TempV = (CardCopy.transform.localScale); 
        CardCopy.transform.SetParent(PlayerHand.transform);
        CardCopy.transform.localScale = TempV;
    }
}