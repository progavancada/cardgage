﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuCanvas : MonoBehaviour {

    private Button WarriorButton;
    private Button MageButton;
    private Button RogueButton;
    private Button PlayButton;

    public string ScenetoLoad;

    void Awake()
    {
        ScenetoLoad = "CardGameTest1";
        foreach (Transform element in this.transform)
        {
            if(element.tag == "Warrior")
            {
                //WarriorButton = element.GetComponent<Button>();
                Button btn = element.GetComponent<Button>();
                Debug.Log(btn);
                btn.onClick.AddListener(delegate () { ChooseDeck(1); });
            }
            else if (element.tag == "Mage")
            {
                //MageButton = element.GetComponent<Button>();
                Button btn2 = element.GetComponent<Button>();
                Debug.Log(btn2);
                btn2.onClick.AddListener(delegate () { ChooseDeck(2); });
            }
            else if (element.tag == "Rogue")
            {
                //RogueButton = element.GetComponent<Button>();
                Button btn3 = element.GetComponent<Button>();
                Debug.Log(btn3);
                btn3.onClick.AddListener(delegate () { ChooseDeck(3); });
            }
            else if (element.tag == "Play")
            {
                PlayButton = element.GetComponent<Button>();
                Button btn4 = PlayButton.GetComponent<Button>();
                Debug.Log(btn4);
                btn4.onClick.AddListener(delegate () { PlayGame(); });
            }
        }
    }

	void Start (){}
	void Update (){}

    void ChooseDeck(int Deck)
    {
        Debug.Log("Play!");
        CardGameManager.control.SeelectDeck(Deck);
    }

    void PlayGame()
    {
        //Load play mode
        SceneManager.LoadScene(ScenetoLoad);
    }
}
